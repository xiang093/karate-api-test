package examples.users;

import com.intuit.karate.junit5.Karate;

class OperateUsersRunner {
    
    @Karate.Test
    Karate testUsers() {
        return new Karate().feature("opearateUser").relativeTo(getClass());
    }

    @Karate.Test
    Karate testUsersBytag() {
        return new Karate().feature("opearateUser").tags("@smoke").relativeTo(getClass());
    }

}
