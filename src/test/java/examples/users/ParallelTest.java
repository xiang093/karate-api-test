package examples.users;

import com.intuit.karate.KarateOptions;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

@KarateOptions(tags = {"~@ignore"})
class TestParallel {

    @Test
    void testParallel() {
        Results results = Runner.parallel(getClass(), 4, "target/surefire-reports");
        assertTrue(results.getFailCount() == 0, results.getErrorMessages());
    }

//    @Test
//    void testParallelByTag() {
//        List<String> tagList= new ArrayList<String>();
//        List<String> pathList= new ArrayList<String>();
//        tagList.add("@smoke");
//        pathList.add(getClass().toString());
//        Results results = Runner.parallel(tagList,pathList, 2, "target/surefire-reports");
//        assertTrue(results.getFailCount() == 0, results.getErrorMessages());
//    }

}