Feature: get user and create user

  Background:
    * callonce print  "only once"
    * print "hello word"
    * url 'https://02a4eb17-4a72-4ed6-906e-912ee9941055.mock.pstmn.io'
    * def myhead = '11222'

  @smoke
  Scenario: get user list and get first user detail info
    Then path 'getUsers'
    And header token = myhead
    And cookies { someKey: 'someValue', foo: 'bar' }
    When method get
    Then status 200
    * def firstName = response[0].name
    And match firstName == 'test'

     #request url: {baseUrl}/getuser?user=test
    Given path 'getUser'
    And param user = 'test'
    And header token = myhead
    When method get
    Then status 200
    And match $ == {'name':'test','age':18}

  Scenario: create a new user
    Given path 'createUser'
    And request {"name":"xiao","age":17}
    And header token = myhead
    When method Post
    Then status 202
    And match $ == {'name':'xiao','age':17}
    And match response == {'name':'xiao','age':17}


  Scenario Outline: whether user exsit
    Given path 'isExist'
    And param name = <name>
    And header token = myhead
    When method get
    Then status 200
    And match $.message contains 'the user is exist'


    Examples:
    |name|
    | 'huang' |
    | 'ming' |


